var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
const cors = require('cors');

var app = express();

//Conexion con neo4j
var neo4j = require('neo4j-driver');

const { request } = require('http');
const { title } = require('process');
const { log } = require('console');



//View
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended:false}));
app.use("/image",express.static(path.join(__dirname, "image")));
app.use(express.static(path.join(__dirname, 'public')));

    //Driver global y login
var prueba ={};
app.post('/login/user', async function(req,res){   
    const username= req.body.name; 
    const password= req.body.password; 
    
    prueba.driver = neo4j.driver(
        'bolt://neo4j_project',
        neo4j.auth.basic(`${username}`,`${password}`)
      )
    
        //console.log(driver);
    prueba.driver.close()
    console.log(username + password);

        res.redirect('/');
});


app.get('/', function(req,res){
    res.render('index');
});


app.get('/movies', function(req,res){
    var session = prueba.driver.session();

    session
    .run('MATCH(n:Movie) RETURN n')
    .then(function(result){
        var movieArr = [];
        result.records.forEach(function(record){
            movieArr.push({
                id: record._fields[0].identity.low,
                title: record._fields[0].properties.title,
                released: record._fields[0].properties.released,
                tagline: record._fields[0].properties.tagline,
                url: record._fields[0].properties.url,

            });
            
        });
        res.render('movie',{
            movies: movieArr
        });
    })
    .catch(function(err){
        console.log(err);
    });

});

app.get('/actors', function(re,res){
    var session = prueba.driver.session();

    session
    .run('MATCH(n:Person) RETURN n')
    .then(function(result2){
        var personArr = [];
        result2.records.forEach(function(record){
            personArr.push({
                id: record._fields[0].identity.low,
                name: record._fields[0].properties.name,
                born: record._fields[0].properties.born,
                url: record._fields[0].properties.url,

            });
        });
        res.render('actors', {
            people: personArr
        });
    })  
    .catch(function(err){
        console.log(err);
    });

});

app.get('/directors', function(re,res){
    var session = prueba.driver.session();

    session
    .run('MATCH(n:Director) RETURN n')
    .then(function(result5){
        var directorArr = [];
        result5.records.forEach(function(record){
            directorArr.push({
                id: record._fields[0].identity.low,
                name: record._fields[0].properties.name,
                born: record._fields[0].properties.born,
                url: record._fields[0].properties.url,

            });
        });
        res.render('directors', {
            directors: directorArr
        });
    })  
    .catch(function(err){
        console.log(err);
    });

});

app.get('/graphs', function(re,res){
    var session = prueba.driver.session();

    session
    .run('MATCH(n) RETURN n')
    .then(function(result3){
        var personMovieArr = [];
        result3.records.forEach(function(record){
            personMovieArr.push({
                id: record._fields[0].identity.low,
                name: record._fields[0].properties.name,
                born: record._fields[0].properties.born,
                url: record._fields[0].properties.url,
                title: record._fields[0].properties.title,
                released: record._fields[0].properties.released,
                tagline: record._fields[0].properties.tagline,
                url: record._fields[0].properties.url,


            });
        });
        res.render('graph', {
            relation: personMovieArr
        });
    })  
    .catch(function(err){
        console.log(err);
    });

});

app.get('/login', function(re,res){
        res.render('login');

});

//PRUEBAAAAAAAAAAAAAAAA
app.get('/formsMovie', function(re,res){
    res.render('formsMovie');

});

app.get('/formsActor', function(re,res){
    res.render('formsActor');

});

app.get('/moviesAndActors/:title', function(req,res){

    var session2 = prueba.driver.session();
    var title=req.params.title;

    session2
    .run('MATCH (n:Person)-[:ACTED_IN]-(m:Movie{title:$titleParam}) RETURN n,m', {titleParam:title})
    .then(function(result4){
        var moviesActors = [];
        result4.records.forEach(function(record){
            moviesActors.push({
                id: record._fields[0].identity.low,
                name: record._fields[0].properties.name,
                born: record._fields[0].properties.born,
                url: record._fields[0].properties.url,
            });
            
        });
        
        res.render('movieAndActor', {
            moAc: moviesActors
        });
    })  
    .catch(function(err){
        console.log(err);
    });

});

app.get('/moviesAndDirectors/:name', function(req,res){

    var session3 = prueba.driver.session();
    var name=req.params.name;

    session3
    .run('MATCH (n:Movie)-[:DIRECTED]-(m:Director{name:$nameParam}) RETURN n,m', {nameParam:name})
    .then(function(result6){
        var moviesDirectors = [];
        result6.records.forEach(function(record){
            moviesDirectors.push({
                id: record._fields[0].identity.low,
                title: record._fields[0].properties.title,
                released: record._fields[0].properties.released,
                tagline: record._fields[0].properties.tagline,
                url: record._fields[0].properties.url,
            });
            
        });
        
        res.render('movieAndDirector', {
            moDic: moviesDirectors
        });
    })  
    .catch(function(err){
        console.log(err);
    });

});

app.post('/movie/add', function(req,res){

    var session4 = prueba.driver.session();
    var title =req.body.title;
    var released = req.body.released;
    var url= req.body.url;
    var tagline =req.body.tagline;    

    session4
        .run('CREATE(n:Movie {title: $titleParam, released: $releasedParam, url: $urlParam, tagline:$taglineParam}) RETURN n', {titleParam:title,releasedParam:released,urlParam:url,taglineParam:tagline})
        .then(function(result){
            res.setHeader('Access-Control-Allow-Origin', '*');
            
            session.close();
        })
        .catch(function(err){
            console.log(err);
        })
        res.redirect('/movies');
});

app.post('/person/add',function(req,res){

    var session5 = prueba.driver.session();
    var name =req.body.name;
    var born = req.body.born;
    var url= req.body.url;

    session5
        .run('CREATE(n:Person {name: $nameParam, born: $bornParam, url: $urlParam}) RETURN n', {nameParam:name,bornParam:born,urlParam:url})
        .then(function(result){
            res.setHeader('Access-Control-Allow-Origin', '*');
            session.close();
        })
        .catch(function(err){
            console.log();
        });
        res.redirect('/actors');
});

app.post('/movie/person/add',function(req,res){

    var session6 = prueba.driver.session();
    var title =req.body.title;
    var name = req.body.name;

    session6
        .run('MATCH(a:Person {name:$nameParam}),(b:Movie{title:$titleParam}) MERGE(a)-[r:ACTED_IN]-(b) RETURN a,b', {titleParam:title,nameParam:name})
        .then(function(result){
            res.setHeader('Access-Control-Allow-Origin', '*');
            session.close();
        })
        .catch(function(err){
            console.log();
        });
        res.redirect('/');
});



app.post('/movie/delete', function(req,res){

    var session7 = prueba.driver.session();
    var title =req.body.title;

    session7
        .run('MATCH(n:Movie {title: $titleParam}) DELETE n', {titleParam:title})
        .then(function(result){
            res.setHeader('Access-Control-Allow-Origin', '*');
            
            session.close();
        })
        .catch(function(err){
            console.log();
        })
        res.redirect('/movies');
});

app.post('/person/delete', function(req,res){

    var session8 = prueba.driver.session();
    var name = req.body.name;

    session8
        .run('MATCH(n:Person {name: $nameParam}) DELETE n', {nameParam:name})
        .then(function(result){
            res.setHeader('Access-Control-Allow-Origin', '*');
            
            session.close();
        })
        .catch(function(err){
            console.log();
        })
        res.redirect('/actors');
});

app.post('/movie/person/delete', function(req,res){

    var session9 = prueba.driver.session();
    var name = req.body.name;

    session9
        .run('MATCH(n:Person {name:$nameParam})-[r]-() DELETE r', {nameParam:name})
        .then(function(result){
            res.setHeader('Access-Control-Allow-Origin', '*');
            
            session.close();
        })
        .catch(function(err){
            console.log();
        })
        res.redirect('/movies');
});

app.listen(3000);
console.log('Server Started on Port 3000');

module.exports = app;

