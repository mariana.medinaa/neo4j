CREATE (Wonder:Movie {title:"Wonder", released:2017, tagline:'You cant try and blend in, When you were born to stand out.', url:'https://i.blogs.es/2305c2/wonder-critica-espinof/450_1000.jpg'})
CREATE (JacobT:Person {name:'Jacob Tremblay', born:2006, url:'https://lanetaneta.com/wp-content/uploads/2021/04/El-reinicio-de-Toxic-Avenger-lanza-a-Jacob-Tremblay.jpeg'})
CREATE (JuliaR:Person {name:'Julia Roberts', born:1967, url:'https://okdiario.com/look/img/2020/10/28/julia-roberts.jpg'})
CREATE (OwenW:Person {name:'Owen Wilson', born:1968, url:'https://media.airedesantafe.com.ar/p/89ae57c893131603501812bd477ab65e/adjuntos/268/imagenes/001/471/0001471955/la-ex-novia-owen-wilson-lo-acusa-no-querer-ver-su-hija.jpg'})
CREATE (NoahJ:Person {name:'Noah Jupe', born:2005, url:'https://www.chismestoday.com/wp-content/uploads/2020/08/FotoJet-58.jpg'})
CREATE (IzabelaV:Person {name:'Izabela Vidovic', born:2001, url:'https://thumbs.dreamstime.com/b/izabela-vidovic-103918630.jpg'})
CREATE (DanielleR:Person {name:'Danielle Rose Russell', born:1999, url:'https://www.yourtango.com/sites/default/files/styles/listing_big/public/image_blog/danielle-rose-russell.jpeg?itok=4Pyo9al3'})
CREATE (StephenC:Director {name:'Stephen Chbosky', born:1970, url:'https://ep01.epimg.net/cultura/imagenes/2019/12/17/actualidad/1576544657_577036_1576544952_noticia_normal.jpg'})
CREATE
  (JacobT)-[:ACTED_IN {roles:['Auggie Pullman']}]->(Wonder),
  (JuliaR)-[:ACTED_IN {roles:['Isabel Pullman']}]->(Wonder),
  (OwenW)-[:ACTED_IN {roles:['Nate Pullman']}]->(Wonder),
  (NoahJ)-[:ACTED_IN {roles:['Jack Will']}]->(Wonder),
  (IzabelaV)-[:ACTED_IN {roles:['Olivia "Vía" Pullman']}]->(Wonder),
  (DanielleR)-[:ACTED_IN {roles:['Miranda']}]->(Wonder),
  (StephenC)-[:DIRECTED]->(Wonder)
CREATE (MeBeforeYou:Movie {title:"Me Before You", released:2016, tagline:"Live boldly.", url:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSnt_ztFnLr6lxYPi8b2Dk466MK8hVbkm4YyA&usqp=CAU'})
CREATE (EmiliaC:Person {name:'Emilia Clarke', born:1986, url:'https://aws.admagazine.com/prod/designs/v1/assets/767x384/81853.jpg'})
CREATE (SamC:Person {name:'Sam Claflin', born:1986, url:'https://www.elespectador.com/resizer/h9TaSJRC0WS2Cd2Sj1sDMxs7bFU=/657x0/arc-anglerfish-arc2-prod-elespectador.s3.amazonaws.com/public/PNQDMQLKYJA6HOHSBGX4TEW4KA.jpg'})
CREATE (JennaC:Person {name:'Jenna Coleman', born:1986, url:'https://seriepolis.com/wp-content/uploads/2019/09/jenna-coleman.jpg'})
CREATE (CharlesD:Person {name:'Charles Dance', born:1946, url:'https://culturageek.com.ar/wp-content/uploads/2021/04/Charles-Dance-CulturaGeek.jpg'})
CREATE (JanetM:Person {name:'Janet McTeer', born:1961, url:'https://www.bolsamania.com/seriesadictos/wp-content/uploads/2017/04/L%C2%B4S.jpg'})
CREATE (MatthewL:Person {name:'Matthew Lewis', born:1989, url:'https://los40es00.epimg.net/los40/imagenes/2018/05/29/love40/1527600416_443160_1527600771_noticia_normal.jpg'})
CREATE (TheaS:Director {name:'Thea Sharrock', born:1971, url:'https://cdn2.actitudfem.com/media/files/thea_sharrock.jpg'})
CREATE
  (EmiliaC)-[:ACTED_IN {roles:['Louisa "Lou" Clark']}]->(MeBeforeYou),
  (SamC)-[:ACTED_IN {roles:['William "Will" Traynor']}]->(MeBeforeYou),
  (JennaC)-[:ACTED_IN {roles:['Katrina "Treena" Clark']}]->(MeBeforeYou),
  (CharlesD)-[:ACTED_IN {roles:['Steven Traynor']}]->(MeBeforeYou),
  (JanetM)-[:ACTED_IN {roles:['Camilla Traynor']}]->(MeBeforeYou),
  (MatthewL)-[:ACTED_IN {roles:['Patrick']}]->(MeBeforeYou),
  (TheaS)-[:DIRECTED]->(MeBeforeYou)
CREATE (ElLaberintoDelFauno:Movie {title:"El Laberinto Del Fauno", released:2006, tagline:'In darkness, there can be light. In misery, there can be beauty. In death, there can be life...', url:'https://images-na.ssl-images-amazon.com/images/I/71A%2BRXry%2B2L._SL1136_.jpg'})
CREATE (IvanaB:Person {name:'Ivana Baquero', born:1994, url:'https://tvteleaudiencias.files.wordpress.com/2021/02/ivana-baquero.png?w=1038&h=576&crop=1'})
CREATE (SergiL:Person {name:'Sergi López', born:1965, url:'https://www.cuartopoder.es/wp-content/uploads/2015/06/sergilopez2.jpg'})
CREATE (MaribelV:Person {name:'Maribel Verdú', born:1970, url:'https://static.mujerhoy.com/noticias/202006/10/media/cortadas/maribel-verdu-maquillaje-chanel-kaXE-U110466815497N0D-644x483@MujerHoy.jpg'})
CREATE (DougJ:Person {name:'Doug Jones', born:1960, url:'https://media.gettyimages.com/photos/actor-doug-jones-attends-the-academy-of-motion-picture-arts-sciences-picture-id887120384?s=594x594'})
CREATE (GuillermoT:Director {name:'Guillermo del Toro', born:1964, url:'https://www.liderempresarial.com/wp-content/uploads/2020/10/GM.jpg'})
CREATE
  (IvanaB)-[:ACTED_IN {roles:['Ofelia']}]->(ElLaberintoDelFauno),
  (SergiL)-[:ACTED_IN {roles:['Capitán Vidal']}]->(ElLaberintoDelFauno),
  (MaribelV)-[:ACTED_IN {roles:['Mercedes']}]->(ElLaberintoDelFauno),
  (DougJ)-[:ACTED_IN {roles:['Fauno y el hombre pálido']}]->(ElLaberintoDelFauno),
  (GuillermoT)-[:DIRECTED]->(ElLaberintoDelFauno),
  (GuillermoT)-[:WROTE]->(ElLaberintoDelFauno)
CREATE (TenThingsIHateAboutYou:Movie {title:"10 Things I Hate About You", released:1999, tagline:"There are so many different ways to hate. Count them yourself.", url:'https://images-na.ssl-images-amazon.com/images/I/91oLU4E9L0L._SL1500_.jpg'})
CREATE (JuliaS:Person {name:'Julia Stiles', born:1981, url:'https://aws.revistavanityfair.es/prod/designs/v1/assets/785x589/56778.jpg'})
CREATE (HeathL:Person {name:'Heath Ledger', born:1979, url:'https://cdn.hobbyconsolas.com/sites/navi.axelspringer.es/public/media/image/2019/04/actor-heath-ledger-habria-cumplido-hoy-40-anos.jpeg'})
CREATE (LarisaO:Person {name:'Larisa Oleynik', born:1981, url:'https://img.peliplat.com/api/resize/v1?imagePath=std/202002/8/1/81b84f3c81211fb291cb385c5f0d5407.jpg'})
CREATE (JosephG:Person {name:'Joseph Gordon-Levitt', born:1981, url:'https://i.blogs.es/d28988/joseph-gordon-levitt-e1558373010566/1366_2000.jpg'})
CREATE (DavidK:Person {name:'David Krumholtz', born:1978, url:'https://media.gettyimages.com/photos/actor-david-krumholtz-visits-the-build-series-to-discuss-the-film-picture-id1185952523?s=612x612'})
CREATE (AndrewK:Person {name:'Andrew Keegan', born:1979, url:'https://mlpnk72yciwc.i.optimole.com/cqhiHLc.WqA8~2eefa/w:auto/h:auto/q:75/https://bleedingcool.com/wp-content/uploads/2021/03/andrew-keegan.jpg'})
CREATE (LarryW:Person {name:'Larry Miller', born:1953, url:'https://www.executivespeakers.com/Upload/Speaker/larry-miller.jpg'})
CREATE (GilJ:Director {name:'Gil Junger', born:1954, url:'https://deadline.com/wp-content/uploads/2020/08/gil-junger-e1598293347571.jpg'})
CREATE
  (JuliaS)-[:ACTED_IN {roles:['Katarina "Kat" Stratford']}]->(TenThingsIHateAboutYou),
  (HeathL)-[:ACTED_IN {roles:['Patrick Verona']}]->(TenThingsIHateAboutYou),
  (LarisaO)-[:ACTED_IN {roles:['Bianca Stratford']}]->(TenThingsIHateAboutYou),
  (JosephG)-[:ACTED_IN {roles:['Cameron James']}]->(TenThingsIHateAboutYou),
  (DavidK)-[:ACTED_IN {roles:['Michael Eckman']}]->(TenThingsIHateAboutYou),
  (AndrewK)-[:ACTED_IN {roles:['Joey Donner']}]->(TenThingsIHateAboutYou),
  (LarryW)-[:ACTED_IN {roles:['Dr. Stratford']}]->(TenThingsIHateAboutYou),
  (GilJ)-[:DIRECTED]->(TenThingsIHateAboutYou)
CREATE (OceansEleven:Movie {title:'Oceans Eleven', released:2001, tagline:'They are Having So Much Fun It is Illegal.', url:'https://es.web.img3.acsta.net/medias/nmedia/00/02/33/84/aff11.jpg'})
CREATE (GeorgeC:Person {name:'George Clooney', born:1961, url:'https://aws.revistavanityfair.es/prod/designs/v1/assets/785x589/211248.jpg'})
CREATE (BradP:Person {name:'Brad Pitt', born:1963, url:'https://cloudfront-eu-central-1.images.arcpublishing.com/prisa/CNPZFVXLZNH2ZITC5MY3LVOF44.jpg'})
CREATE (MattD:Person {name:'Matt Damon', born:1970, url:'https://www.revanchamag.com/wp-content/uploads/2020/10/Matt-Damon-1280x640.jpg'})
CREATE (AndyG:Person {name:'Andy García', born:1956, url:'https://i1.wp.com/www.sectorcine.com/wp-content/uploads/2020/04/andy-garcia-mejores-peliculas.jpg?fit=700%2C400&ssl=1'})
CREATE (StevenS:Director {name:'Steven Soderbergh', born:1963, url:'https://lavozdgo.com/wp-content/uploads/2020/12/24203_01-intervista-a-steven-soderbergh.jpg'})
CREATE
  (GeorgeC)-[:ACTED_IN {roles:['Daniel "Danny" Ocean']}]->(OceansEleven),
  (BradP)-[:ACTED_IN {roles:['Rusty Ryan']}]->(OceansEleven),
  (MattD)-[:ACTED_IN {roles:['Linus Caldwell']}]->(OceansEleven),
  (AndyG)-[:ACTED_IN {roles:['Terry Benedict']}]->(OceansEleven),
  (JuliaR)-[:ACTED_IN {roles:['Tess Ocean']}]->(OceansEleven),
  (StevenS)-[:DIRECTED]->(OceansEleven)
CREATE (TheMonumentsMen:Movie {title:'The Monuments Men', released:2014, tagline:'It was the greatest art heist in history', url:'http://3.bp.blogspot.com/-Q3byc5FbatY/UiIPGYvtLSI/AAAAAAAAJWE/CMJwfedd4Lg/s1600/monuments+men+alternative.jpg'})
CREATE (BillM:Person {name:'Bill Murray', born:1950, url:'https://www.eleconomista.com.mx/__export/1507906109271/sites/eleconomista/img/historico/wp-bill-murray-ap.png_1612941218.png'})
CREATE (JohnG:Person {name:'John Goodman', born:1952, url:'https://www.bolsamania.com/seriesadictos/wp-content/uploads/2018/05/john-goodman.jpg'})
CREATE (CateB:Person {name:'Cate Blanchett', born:1969, url:'https://as01.epimg.net/tikitakas/imagenes/2020/05/29/portada/1590745399_117314_1590747856_sumario_normal.jpg'})
CREATE
  (GeorgeC)-[:ACTED_IN {roles:['Frank Stokes']}]->(TheMonumentsMen),
  (MattD)-[:ACTED_IN {roles:['James Granger']}]->(TheMonumentsMen),
  (BillM)-[:ACTED_IN {roles:['Richard Campbell']}]->(TheMonumentsMen),
  (JohnG)-[:ACTED_IN {roles:['Walter Garfield']}]->(TheMonumentsMen),
  (CateB)-[:ACTED_IN {roles:['Claire Simone']}]->(TheMonumentsMen),
  (GeorgeC)-[:DIRECTED]->(TheMonumentsMen)
CREATE (YouveGotMail:Movie {title:"You've Got Mail", released:1998, tagline:'At odds in life... in love on-line.', url:'https://images-na.ssl-images-amazon.com/images/I/5142FW33AJL._SY445_.jpg'})
CREATE (ParkerP:Person {name:'Parker Posey', born:1968, url:'https://www.bolsamania.com/seriesadictos/wp-content/uploads/2016/11/parker-posey-lost-in-space-netflix.jpg'})
CREATE (DaveC:Person {name:'Dave Chappelle', born:1973, url:'https://elrincondenetflix.com/wp-content/uploads/2020/06/dave-chappelle.jpg'})
CREATE (SteveZ:Person {name:'Steve Zahn', born:1967, url:'https://www.bolsamania.com/seriesadictos/wp-content/uploads/2017/03/steve-zahn-2.jpg'})
CREATE (TomH:Person {name:'Tom Hanks', born:1956, url:'https://www.sectorcine.com/wp-content/uploads/2019/07/las-peliculas-de-tom-hanks.jpg'})
CREATE (GregK:Person {name:'Greg Kinnear', born:1963, url:'https://variety.com/wp-content/uploads/2018/11/greg-kinnear.jpg?w=1000'})
CREATE (MegR:Person {name:'Meg Ryan', born:1961, url:'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/meg-ryan-arrives-at-the-academy-of-motion-picture-arts-and-news-photo-1605641052.?crop=1.00xw:0.408xh;0,0.0104xh&resize=1200:*'})
CREATE (NoraE:Director {name:'Nora Ephron', born:1941, url:'https://d3iln1l77n73l7.cloudfront.net/couch_images/attachments/000/083/621/original/nora-ephron.jpg?2016'})
CREATE
  (TomH)-[:ACTED_IN {roles:['Joe Fox']}]->(YouveGotMail),
  (MegR)-[:ACTED_IN {roles:['Kathleen Kelly']}]->(YouveGotMail),
  (GregK)-[:ACTED_IN {roles:['Frank Navasky']}]->(YouveGotMail),
  (ParkerP)-[:ACTED_IN {roles:['Patricia Eden']}]->(YouveGotMail),
  (DaveC)-[:ACTED_IN {roles:['Kevin Jackson']}]->(YouveGotMail),
  (SteveZ)-[:ACTED_IN {roles:['George Pappas']}]->(YouveGotMail),
  (NoraE)-[:DIRECTED]->(YouveGotMail)
CREATE (SleeplessInSeattle:Movie {title:'Sleepless in Seattle', released:1993, tagline:'What if someone you never met, someone you never saw, someone you never knew was the only someone for you?', url:'https://images-na.ssl-images-amazon.com/images/I/71JivGlSBYL._SL1500_.jpg'})
CREATE (RitaW:Person {name:'Rita Wilson', born:1956, url:'https://phantom-marca.unidadeditorial.es/b55c926795bab81a1e702ba24ea3edfd/resize/1300/assets/multimedia/imagenes/2020/03/12/15840152162323.jpg'})
CREATE (BillPull:Person {name:'Bill Pullman', born:1953, url:'https://es.web.img2.acsta.net/r_1280_720/pictures/20/06/09/16/59/1526328.jpg'})
CREATE (VictorG:Person {name:'Victor Garber', born:1949, url:'https://post.healthline.com/wp-content/uploads/2020/09/VictorGarber-2018-1200x628.jpg'})
CREATE (RosieO:Person {name:"Rosie O'Donnell", born:1962, url:'https://images.clarin.com/2014/11/01/rJMFC9jhml_1256x620.jpg'})
CREATE
  (TomH)-[:ACTED_IN {roles:['Sam Baldwin']}]->(SleeplessInSeattle),
  (MegR)-[:ACTED_IN {roles:['Annie Reed']}]->(SleeplessInSeattle),
  (RitaW)-[:ACTED_IN {roles:['Suzy']}]->(SleeplessInSeattle),
  (BillPull)-[:ACTED_IN {roles:['Walter']}]->(SleeplessInSeattle),
  (VictorG)-[:ACTED_IN {roles:['Greg']}]->(SleeplessInSeattle),
  (RosieO)-[:ACTED_IN {roles:['Becky']}]->(SleeplessInSeattle),
  (NoraE)-[:DIRECTED]->(SleeplessInSeattle)
CREATE (JoeVersustheVolcano:Movie {title:'Joe Versus the Volcano', released:1990, tagline:'A story of love, lava and burning desire.', url:'https://images-na.ssl-images-amazon.com/images/I/512ARPgfr0L.jpg'})
CREATE (JohnS:Director {name:'John Patrick Stanley', born:1950, url:'https://i0.wp.com/guyatthemovies.com/wp-content/uploads/2021/02/johnpatrickshanley-compressed.jpg?fit=1296%2C730&ssl=1'})
CREATE (Nathan:Person {name:'Nathan Lane', born:1956, url:'https://www.goldderby.com/wp-content/uploads/2020/04/Nathan-Lane.jpg'})
CREATE
  (TomH)-[:ACTED_IN {roles:['Joe Banks']}]->(JoeVersustheVolcano),
  (MegR)-[:ACTED_IN {roles:['DeDe', 'Angelica Graynamore', 'Patricia Graynamore']}]->(JoeVersustheVolcano),
  (Nathan)-[:ACTED_IN {roles:['Baw']}]->(JoeVersustheVolcano),
  (JohnS)-[:DIRECTED]->(JoeVersustheVolcano)
CREATE (ThatThingYouDo:Movie {title:'That Thing You Do', released:1996, tagline:'In every life there comes a time when that thing you dream becomes that thing you do', url:'https://images-na.ssl-images-amazon.com/images/I/81UucPeZ%2BZL._SL1200_.jpg'})
CREATE (LivT:Person {name:'Liv Tyler', born:1977, url:'https://hips.hearstapps.com/es.h-cdn.co/fotoes/images/noticias-cine/liv-tyler-a-mi-edad-eres-una-ciudadana-de-segunda-en-hollywood/73758335-1-esl-ES/Liv-Tyler-a-mi-edad-eres-una-ciudadana-de-segunda-en-Hollywood.jpg?crop=1xw:0.6666666666666666xh;center,top&resize=1200:*'})
CREATE (Charlize:Person {name:'Charlize Theron', born:1975, url:'https://images.chicmagazine.com.mx/wFdMGKpkNL3VrLmMlvzo5e38FFg=/958x596/uploads/media/2020/08/05/charlize-theron-mujer-penn-rompio.jpg'})
CREATE
  (TomH)-[:ACTED_IN {roles:['Mr. White']}]->(ThatThingYouDo),
  (LivT)-[:ACTED_IN {roles:['Faye Dolan']}]->(ThatThingYouDo),
  (Charlize)-[:ACTED_IN {roles:['Tina']}]->(ThatThingYouDo),
  (TomH)-[:DIRECTED]->(ThatThingYouDo)
CREATE (TheDaVinciCode:Movie {title:'The Da Vinci Code', released:2006, tagline:'Break The Codes', url:'https://cl.buscafs.com/www.tomatazos.com/public/uploads/images/68413/68413_800x1189.jpg'})
CREATE (IanM:Person {name:'Ian McKellen', born:1939, url:'https://espanol.news24viral.com/wp-content/uploads/2021/02/Como-se-siente-Sir-Ian-McKellan-despues-de-recibir-la.jpg'})
CREATE (AudreyT:Person {name:'Audrey Tautou', born:1976, url:'https://besthqwallpapers.com/Uploads/1-2-2018/39146/thumb2-audrey-tautou-french-actress-portrait-smile-photoshoot.jpg'})
CREATE (PaulB:Person {name:'Paul Bettany', born:1971, url:'https://espanol.news24viral.com/wp-content/uploads/2021/02/Paul-Bettany-El-actor-de-WandaVision-vale-mas-de-lo.jpg'})
CREATE (RonH:Director {name:'Ron Howard', born:1954, url:'https://imagenes.20minutos.es/files/image_656_370/uploads/imagenes/2015/11/28/ron-howard1.jpg'})
CREATE
  (TomH)-[:ACTED_IN {roles:['Dr. Robert Langdon']}]->(TheDaVinciCode),
  (IanM)-[:ACTED_IN {roles:['Sir Leight Teabing']}]->(TheDaVinciCode),
  (AudreyT)-[:ACTED_IN {roles:['Sophie Neveu']}]->(TheDaVinciCode),
  (PaulB)-[:ACTED_IN {roles:['Silas']}]->(TheDaVinciCode),
  (RonH)-[:DIRECTED]->(TheDaVinciCode)
;